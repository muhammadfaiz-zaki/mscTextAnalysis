# -*- coding: utf-8 -*-
from bs4 import BeautifulSoup
import requests
import os
import re ## Regular expressions
import time
import unicodedata



def remove_accents(input_str):
    nfkd_form = unicodedata.normalize('NFKD', input_str)
    return u"".join([c for c in nfkd_form if not unicodedata.combining(c)])

## Function that replace vowels with accents
def replaceFrenchChar(string):
    string = re.sub('[áâà]','a', string)
    string = re.sub('[ÁÂÀ]','A', string)
    string = re.sub('[éêè]','e', string)
    string = re.sub('[ÉÈ]','E', string)
    string = re.sub('[íî]','i', string)
    string = re.sub('[ÌÍÎ]','I', string)
    string = re.sub('[ÓÒÕÔ]','o', string)
    string = re.sub('[úûù]','u', string)
    string = re.sub('[ÚÛÙÜ]','U', string)
    string = re.sub('[ç]','c', string)
    string = re.sub('[ÇĆ]','C', string)
    string = re.sub('\n','', string)
    return string


### I loop over each web article that DOES NOT is not in "Location, date format"

# Step 1: Get the right url and try to request it, then get the content and make 
#            a soup object. 
#            Jump back to beginning of the loop, if
#            a) the website does not exist
#            b) the website has no content
# Step 2: Get all the dates and append to file
# Step 3: Put the title in the next line of the file
# Step 4: Put the all paragraph in the file
# Step 5: Get special articles that do not start with a date



# Step 1 Preparation:
for articleNum in range(1,20):
    print ("Start with " + str(articleNum) + ": " + time.strftime("%H:%M:%S"))
    url = "www.onuci.org/spip.php?article" + str(articleNum)
    print url
    try:
        r  = requests.get("http://" +url)
        data = r.content
        soup = BeautifulSoup(data, "html.parser") # htlm5lib as alternative parser
        
        # Make folder name if there are paragrapgs/text in website
        if (soup.find_all('p')!= []):
            print("Text found in article " + str(articleNum))
            folder = str(os.getcwd()) + str(articleNum) + ".txt" 
            text = open(folder, 'w')
            text.close()
            text = open(folder, 'a')
            print(folder)

        # Title
        text.write("Title: ")
        btag = soup.find_all('b')
        title = btag[0].get_text()  
        title = remove_accents(title)   ## Title is most likely first or second b tag
        byte_title = title.encode(encoding='UTF8').lstrip()
        #byte_title = replaceFrenchChar(string = byte_title)
        text.write(byte_title)
                

        # Date
        text.write(str("\nDate: "))

        dateLongFR = str(r'\s([0-9]{1}|[0-9]{2})\s([jJ]anvier|[fF]évrier|[mM]ars|[aA]vril|[mM]ai|[jJ]uin|[jJ]uillet|[aA]oût|[sS]eptembre|[oO]ctobre|[nN]ovembre|[dD]écembre)\s(\d{4})')
        dateLongEN = str(r'\s([0-9]{1}|[0-9]{2})\s([jJ]anuary|[fF]ebruary|[mM]arch|[aA]pril|[mM]ay|[jJ]une|[jJ]uly|[aA]ugust|[sS]eptember|[oO]ctober|[nN]ovember|[dD]ecember)\s(\d{4})')
        dateShort = str(r'([0-1]{1}|[0-9]{2}/)([0-1]{1}|[0-9]{2}/)([0-9]{4})')

        for p in soup.find_all('p'):                    
            if (re.search(dateLongFR, p.get_text() ) ):
                dateMatch=re.search(dateLongFR, p.get_text() )
                date = dateMatch.group(1) + dateMatch.group(2) + dateMatch.group(3)
                date  = replaceFrenchChar(string = date)
                text.write(str(date) + ",")
            elif (re.search(dateLongEN, p.get_text() ) ):
                dateMatch=re.search(dateLongEN, p.get_text() )
                date = dateMatch.group(1) + dateMatch.group(2) + dateMatch.group(3)
                date = replaceFrenchChar(string = date)
                text.write(str(date) + ",")
            elif (re.search(dateShort, p.get_text() ) ):
                dateMatch=re.search(dateShort, p.get_text() )
                date = dateMatch.group(1) + dateMatch.group(2) + dateMatch.group(3)
                date = replaceFrenchChar(string = date)
                text.write(str(date) + ",")


        # Paragraph
        text.write(str("\nText: "))
        ptag = soup.find_all('p')
        textUNI = ptag[1].get_text()  ## Main content at second p tag
        textUNI = remove_accents(textUNI)   
        byte_text = textUNI.encode(encoding='UTF8').lstrip() \
                    .replace("Repondre a cet article ", "") \
                    .replace("Voir les photos", "")
                    
        text.write(byte_text)
       
        text.close()


    except:
        print("Article "+ str(articleNum) + " has not paragraphs/texts or does not exist.")
        continue
