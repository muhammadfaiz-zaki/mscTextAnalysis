# -*- coding: utf-8 -*-
'''
Created on Mar 19, 2016

@author: mfaizmzaki
'''

from nltk.tokenize import RegexpTokenizer
from stop_words import get_stop_words
from nltk.stem.porter import PorterStemmer
from gensim import corpora, models
import codecs
import os

#initialize tokenizer
tokenizer = RegexpTokenizer(r'\w+')
   
#create English and French stop words list
fr_stop = get_stop_words('french')
en_stop = get_stop_words('english')
 
# Create p_stemmer of class PorterStemmer
p_stemmer = PorterStemmer()

#path of dataset  
path = "/Users/mfaizmzaki/eclipse_workspace/peacekeeping/text"

#list to store dataset
doc_set = []

#iterate through the directory
#python open file upper limit error above 10200 files. Still working on it.
dirs = os.listdir(path)[:1000]

#add files to doc_set
for files in dirs: 
    doc = codecs.open(path + "/" + files, encoding='utf-8')
    doc_set.append(doc)
        

#list for tokenized documents in loop
texts = []
  
  
for i in doc_set:
    for j,line in enumerate(i):
    # clean and tokenize document string
        if j > 1:
            raw = line.lower()
            tokens = tokenizer.tokenize(raw)
            
    # remove stop words from tokens
            stopped_tokens = [i for i in tokens if not i in fr_stop]
            stopped_tokens2 = [i for i in stopped_tokens if not i in en_stop]
          
    # stem tokens
            stemmed_tokens = [p_stemmer.stem(i) for i in stopped_tokens2]
          
    # add tokens to list
            texts.append(stopped_tokens2)
            
#turn our tokenized documents into a id <-> term dictionary
dictionary = corpora.Dictionary(texts)
      
#convert tokenized documents into a document-term matrix
corpus = [dictionary.doc2bow(text) for text in texts]
       
       
# generate LDA model
ldamodel = models.ldamodel.LdaModel(corpus, num_topics=20, id2word = dictionary, passes=500)

#print topics 
for i in ldamodel.print_topics(num_words=8):
    print i[0], i[1]
    
      
      


